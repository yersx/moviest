apply plugin: 'com.android.application'

apply plugin: 'kotlin-android'

apply plugin: 'kotlin-android-extensions'

apply plugin: 'kotlin-kapt'

apply plugin: 'androidx.navigation.safeargs.kotlin'

android {
    compileSdkVersion 29
    buildToolsVersion "29.0.1"
    defaultConfig {
        applicationId "kz.moviest.app"
        minSdkVersion 21
        targetSdkVersion 29
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"

        vectorDrawables.useSupportLibrary = true
        multiDexEnabled true
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }

    flavorDimensions "default"

    productFlavors {
        dev {
            dimension "default"
            applicationId "kz.moviest.app_dev"
            buildConfigField "String", "URL_BASE", '"https://api.themoviedb.org/3/movie/"'
            buildConfigField "boolean", "IS_DEBUG", "true"
            buildConfigField "String", "FILE_PROVIDER", '"kz.moviest.app_dev.fileprovider"'
            manifestPlaceholders = [manifestFileProvider: "kz.moviest.app_dev.fileprovider"]
        }
    }

    dataBinding {
        enabled = true
    }

    androidExtensions {
        experimental = true
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    bundle {
        language {
            // Specifies that the app bundle should not support
            // configuration APKs for language resources. These
            // resources are instead packaged with each base and
            // dynamic feature APK.
            enableSplit = false
        }
    }
}

apply from: '../versions.gradle'

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])

    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'androidx.lifecycle:lifecycle-extensions:2.0.0'
    testImplementation "junit:junit:$jUnitVersion"
    androidTestImplementation "androidx.test:runner:$testRunnerVersion"
    androidTestImplementation "androidx.test.espresso:espresso-core:$espressoCoreVersion"

    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"

    implementation "androidx.appcompat:appcompat:$androidxAppcompat"
    implementation "androidx.constraintlayout:constraintlayout:$androidxConstraintlayout"
    implementation "androidx.recyclerview:recyclerview:$androidxRecyclerview"
    implementation "androidx.cardview:cardview:$androidxCardview"
    implementation "androidx.multidex:multidex:$androidxMultidex"

    implementation "com.google.android.material:material:$androidMaterial"
    implementation "com.google.android:flexbox:$androidFlexbox"

    //KTX
    implementation "androidx.core:core-ktx:$androidxCoreKtx"
    implementation "androidx.fragment:fragment-ktx:$fragmentKtx"

    //ARCH COMPONENTS
    //-> ViewModel and LiveData
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.1.0-beta01"
    implementation "androidx.lifecycle:lifecycle-runtime-ktx:2.2.0-alpha01"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:2.2.0-alpha01"

    kapt "androidx.lifecycle:lifecycle-compiler:$lifecycleVersion"

    //Pagination
    implementation "androidx.paging:paging-runtime:2.1.2"


    //-> scheduler
    implementation "android.arch.work:work-runtime:$workRuntimeVersion"

    //-> navigation
    implementation "androidx.navigation:navigation-fragment-ktx:$navigationVersion"
    implementation "androidx.navigation:navigation-ui-ktx:$navigationVersion"

    //NETWORK
    //-> retrofit
    implementation "com.squareup.retrofit2:retrofit:$retrofit2Version"
    implementation "com.squareup.retrofit2:converter-gson:$retrofit2Version"

    //-> okhttp
    implementation "com.squareup.okhttp3:okhttp:$okhttp3"
    implementation "com.squareup.okhttp3:logging-interceptor:$okhttp3LoggingInterceptor"

    //DAGGER 2
    implementation "com.google.dagger:dagger:$dagger2Version"
    kapt "com.google.dagger:dagger-compiler:$dagger2Version"

    //-> dagger.android package (optional)
    implementation "com.google.dagger:dagger-android:$dagger2Version"
    kapt "com.google.dagger:dagger-android-processor:$dagger2Version"

    //-> Support library support (optional)
    implementation "com.google.dagger:dagger-android-support:$dagger2Version" //if you use the support libraries

    //IMAGE
    implementation "com.github.bumptech.glide:glide:$glideVersion"
    kapt "com.github.bumptech.glide:compiler:$glideVersion"

    implementation "jp.wasabeef:glide-transformations:$glideTransformationsVersion"

    //UTILS
    implementation "com.redmadrobot:inputmask:$inputMaskVersion"
    implementation "com.romandanylyk:pageindicatorview:$pageindicatorviewVersion"

    implementation "net.yslibrary.keyboardvisibilityevent:keyboardvisibilityevent:2.3.0"

    //Coroutines
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutineVersion"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutineVersion"


}